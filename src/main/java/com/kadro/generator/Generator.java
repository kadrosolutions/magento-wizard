package com.kadro.generator;

public interface Generator {

    public default String generateAttribute(String attribute, String value) {
        return generateAttribute(attribute, value, true);
    }

    public default String generateAttribute(String attribute, String value, boolean appendSpace) {
        return new StringBuilder(attribute).append("=\"").append(value).append("\"").append(appendSpace ? " ":"").toString();
    }

    public default String generateAttribute(String attribute, int value) {
        return generateAttribute(attribute, String.valueOf(value));
    }

    public default String generateAttribute(String attribute, boolean value) {
        return generateAttribute(attribute, String.valueOf(value));
    }
}
