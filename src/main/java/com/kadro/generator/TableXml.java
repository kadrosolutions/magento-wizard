package com.kadro.generator;

import com.kadro.schemaAnalyzer.SchemaAnalyzer;
import com.kadro.schemaAnalyzer.intf.*;
import com.sun.tools.internal.jxc.ap.Const;

import java.io.StringWriter;
import java.util.Collections;
import java.util.function.Function;

public class TableXml implements Generator {

    public static void main(String[] args) {
        Table table = SchemaAnalyzer.analyzeTable("kadro_onboarding_product_link", "kadro_tal_onboarding", "def", ConnectionProperties.build(
                "com.mysql.cj.jdbc.Driver"
                , "jdbc:mysql://10.10.10.22:32892"
                , "kadro"
                , "kadro"
                ,"kadro_tal_onboarding"
        ));
        new TableXml(table).generate();
    }

    private static final String NAME = "name";
    private static final String XSI_TYPE = "xsi:type";
    private static final String REFERENCE_ID = "referenceId";

    private Table table; 

    public TableXml(Table table) {
        this.table = table;
    }

    public Table getTable() {
        return this.table;
    }

    public void generate() {
        StringWriter writer = new StringWriter();
        writer.append("<table ").append(generateAttribute(NAME, getTable().getName(), false)).append(">").append(System.lineSeparator());

        for (Column column : getTable().getColumns()) {
            writer.append("\t").append(generateColumn(column)).append(System.lineSeparator());
        }

        writer.append(System.lineSeparator());

        writer.append(generateConstraint(Constraint.Type.PRIMARY_KEY, this::generatePrimaryConstraint));
        writer.append(generateConstraint(Constraint.Type.UNIQUE, this::generateUniqueConstraint));
        writer.append(generateConstraint(Constraint.Type.FOREIGN_KEY, this::generateForeignConstraint));

        for (Index index : table.getIndexes()) {
            writer.append("\t").append(generateIndex(index)).append(System.lineSeparator());
        }

        writer.append(System.lineSeparator()).append("</table>");

        System.out.println(writer.toString());
    }

    private String generateConstraint(Constraint.Type type, Function<Constraint, String> generator) {
        StringBuilder sb = new StringBuilder();

        boolean found = false;

        for (Constraint constraint : getTable().getConstraintsByType().getOrDefault(type, Collections.emptyList())) {
            sb.append("\t").append(generator.apply(constraint));
            sb.append(System.lineSeparator());
            found = true;
        }

        if (found) {
            sb.append(System.lineSeparator());
        }

        return sb.toString();
    }

    public String generateColumn(Column column) {
        StringBuilder sb = new StringBuilder();
        sb.append("<column ");
        sb.append(generateAttribute(NAME, column.getName()));

        sb.append(generateAttribute(XSI_TYPE, column.getDataType().getName().toLowerCase()));

        if (column.isOnUpdate()) {
            sb.append(generateAttribute("on_update", true));
        }

        if (column.getPrecision() != 0) {
            sb.append(generateAttribute("padding", column.getPrecision()));
        }

        if (column.isUnsigned()) {
            sb.append(generateAttribute("unsigned", true));
        }

        if (column.isAutoIncrement()) {
            sb.append(generateAttribute("identity", true));
        }

        if (column.getDefault() != null) {
            sb.append(generateAttribute("default", column.getDefault().toLowerCase()));
        }

        sb.append("/>");

        return sb.toString();
    }

    public String generatePrimaryConstraint(Constraint constraint) {
        if (constraint.getType() != Constraint.Type.PRIMARY_KEY) { return ""; }
        StringBuilder sb = new StringBuilder();
        sb.append("<constraint ");
        sb.append(generateAttribute(XSI_TYPE, getXSIType(Constraint.Type.PRIMARY_KEY)));
        sb.append(generateAttribute(REFERENCE_ID, "PRIMARY", false));
        sb.append(">");
        for (String columnName : constraint.getColumnNames()) {
            sb.append(System.lineSeparator()).append("\t\t<column ");
            sb.append(generateAttribute(NAME, columnName));
            sb.append("/>");
        }
        sb.append(System.lineSeparator()).append("\t").append("</constraint>");
        return sb.toString();
    }

    public String generateUniqueConstraint(Constraint constraint) {
        if (constraint.getType() != Constraint.Type.UNIQUE) { return ""; }
        StringBuilder sb = new StringBuilder();
        sb.append("<constraint ");
        sb.append(generateAttribute(XSI_TYPE, getXSIType(Constraint.Type.UNIQUE)));
        sb.append(generateAttribute(REFERENCE_ID, constraint.getName(), false));
        sb.append(">");
        for (String columnName : constraint.getColumnNames()) {
            sb.append(System.lineSeparator()).append("\t\t<column ");
            sb.append(generateAttribute(NAME, columnName));
            sb.append("/>");
        }
        sb.append(System.lineSeparator()).append("\t").append("</constraint>");
        return sb.toString();
    }

    public String generateForeignConstraint(Constraint constraint) {
        if (constraint.getType() != Constraint.Type.FOREIGN_KEY) { return ""; }
        StringBuilder sb = new StringBuilder();
        sb.append("<constraint ");
        sb.append(generateAttribute(XSI_TYPE, getXSIType(Constraint.Type.FOREIGN_KEY)));
        sb.append(generateAttribute(REFERENCE_ID, constraint.getName()));
        sb.append(System.lineSeparator()).append("\t\t\t\t");
        sb.append(generateAttribute("table", getTable().getName()));
        sb.append(generateAttribute("column", constraint.getColumnNames().iterator().next()));
        sb.append(generateAttribute("referenceTable", constraint.getReferencedTableName()));
        sb.append(System.lineSeparator()).append("\t\t\t\t");
        sb.append(generateAttribute("referenceColumn", constraint.getReferencedColumnName()));
        sb.append(generateAttribute("onDelete", constraint.getDeleteRule()));
        sb.append("/>");
        return sb.toString();
    }

    public String generateIndex(Index index) {
        StringBuilder sb = new StringBuilder();
        sb.append("<index ");
        sb.append(generateAttribute("indexType", index.getType().name().toLowerCase()));
        sb.append(generateAttribute(REFERENCE_ID, index.getName()));
        sb.append(">");
        for (String columnName : index.getColumnNames()) {
            sb.append(System.lineSeparator()).append("\t\t<column ");
            sb.append(generateAttribute(NAME, columnName));
            sb.append(" />");
        }
        sb.append(System.lineSeparator()).append("\t").append("</index>");
        return sb.toString();
    }

    public String getXSIType(Constraint.Type type) {
        switch (type) {
            case FOREIGN_KEY: return "foreign";
            case PRIMARY_KEY: return "primary";
            default: return type.name().toLowerCase();
        }
    }

}
