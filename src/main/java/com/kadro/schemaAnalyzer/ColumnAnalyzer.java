package com.kadro.schemaAnalyzer;

import com.kadro.db.mysql.QueryHelper;
import com.kadro.schemaAnalyzer.intf.Column;
import com.kadro.schemaAnalyzer.intf.ConnectionProperties;
import com.kadro.schemaAnalyzer.intf.Table;
import com.mysql.cj.MysqlType;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class ColumnAnalyzer {

    private ColumnAnalyzer() { }

    private enum Columns {
        COLUMN_NAME
        , COLUMN_DEFAULT
        , IS_NULLABLE
        , DATA_TYPE
        , NUMERIC_PRECISION
        , NUMERIC_SCALE
        , DATETIME_PRECISION
        , EXTRA
        , COLUMN_TYPE
        ;
    }

    public static Collection<Column> analyze(String name, String schema, String catalog, ConnectionProperties connectionProperties) {
        return QueryHelper.query(
                connectionProperties
                , getResultGenerator(name, schema, catalog)
                , getResultHandler()
        );
    }

    private static QueryHelper.ResultSetGenerator getResultGenerator(String name, String schema, String catalog) {
        return (connection) -> {
            StringBuffer sql = new StringBuffer("SELECT ");
            sql.append(
                    Arrays.stream(Columns.values())
                            .map(Columns::name)
                            .collect(Collectors.joining(","))
            );
            sql.append(" FROM information_schema.COLUMNS WHERE TABLE_NAME = ? AND TABLE_SCHEMA = ? AND TABLE_CATALOG = ?");

            PreparedStatement statement = connection.prepareStatement(sql.toString());

            statement.setString(1, name);
            statement.setString(2, schema);
            statement.setString(3, catalog);

            return statement.executeQuery();
        };
    }

    private static QueryHelper.ResultSetHandler<Collection<Column>> getResultHandler() {
        Collection<Column> columns = new ArrayList<>();
        return (resultSet) -> {
            while (resultSet.next()) {
                columns.add(
                        new Column.Builder()
                                .setName(resultSet.getString(Columns.COLUMN_NAME.name()))
                                .setDefault(resultSet.getString(Columns.COLUMN_DEFAULT.name()))
                                .setNullable("YES".equalsIgnoreCase(resultSet.getString(Columns.IS_NULLABLE.name())))
                                .setDataType(MysqlType.getByName(resultSet.getString(Columns.DATA_TYPE.name())))
                                .setPrecision(resultSet.getInt(Columns.NUMERIC_PRECISION.name()))
                                .setScale(resultSet.getInt(Columns.NUMERIC_SCALE.name()))
                                .setExtra(resultSet.getString(Columns.EXTRA.name()))
                                .setColumnType(resultSet.getString(Columns.COLUMN_TYPE.name()))
                                .build()
                );
            }

            return columns;
        };
    }
}
