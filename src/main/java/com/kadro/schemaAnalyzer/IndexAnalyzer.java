package com.kadro.schemaAnalyzer;

import com.kadro.db.mysql.QueryHelper;
import com.kadro.schemaAnalyzer.intf.ConnectionProperties;
import com.kadro.schemaAnalyzer.intf.Index;

import java.sql.PreparedStatement;
import java.util.*;
import java.util.stream.Collectors;

public class IndexAnalyzer {
    private IndexAnalyzer() {}

    private enum Columns {
        INDEX_NAME
            , COLUMN_NAMES() {
                @Override
                public String getSelector() {
                    return "GROUP_CONCAT(COLUMN_NAME ORDER BY COLUMN_NAME) as " + name();
                }
            }
            , INDEX_TYPE
        ;

        public String getSelector() {
            return name();
        }
    }

    public static Collection<Index> analyze(String name, String schema, String catalog, ConnectionProperties connectionProperties) {
        return QueryHelper.query(
                connectionProperties
                , getResultGenerator(name, schema, catalog)
                , getResultHandler()
        );
    }

    private static QueryHelper.ResultSetGenerator getResultGenerator(String name, String schema, String catalog) {
        return (connection) -> {
            StringBuffer sql = new StringBuffer("SELECT ");
            sql.append(
                    Arrays.stream(IndexAnalyzer.Columns.values())
                            .map(IndexAnalyzer.Columns::getSelector)
                            .collect(Collectors.joining(","))
            );
            sql.append(" FROM information_schema.STATISTICS s ");
            sql.append("WHERE TABLE_NAME = ? AND TABLE_SCHEMA = ? AND TABLE_CATALOG = ? ");
            sql.append("GROUP BY INDEX_NAME ");
            sql.append(", INDEX_TYPE ");
            sql.append(", TABLE_NAME ");

            PreparedStatement statement = connection.prepareStatement(sql.toString());

            statement.setString(1, name);
            statement.setString(2, schema);
            statement.setString(3, catalog);

            return statement.executeQuery();
        };
    }

    private static QueryHelper.ResultSetHandler<Collection<Index>> getResultHandler() {
        return (resultSet) -> {

            Collection<Index> indexes = new ArrayList<>();

            while (resultSet.next()) {
                indexes.add(new Index.Builder()
                        .setName(resultSet.getString(Columns.INDEX_NAME.name()))
                        .setColumnNames(Arrays.asList(resultSet.getString(IndexAnalyzer.Columns.COLUMN_NAMES.name()).split(",")))
                        .setType(Index.Type.valueOf(resultSet.getString(IndexAnalyzer.Columns.INDEX_TYPE.name())))
                        .build()
                );
            }

            return indexes;
        };
    }

}

