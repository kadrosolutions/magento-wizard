package com.kadro.schemaAnalyzer;

import com.kadro.db.mysql.QueryHelper;
import com.kadro.schemaAnalyzer.intf.Column;
import com.kadro.schemaAnalyzer.intf.ConnectionProperties;
import com.kadro.schemaAnalyzer.intf.Constraint;
import com.kadro.schemaAnalyzer.intf.Table;
import com.mysql.cj.MysqlType;

import java.sql.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class SchemaAnalyzer {

    public static Table analyzeTable(String name, String schema, String catalog, ConnectionProperties properties) {
        Table.Builder tableBuilder = new Table.Builder(name, schema, catalog);

        tableBuilder.setColumns(ColumnAnalyzer.analyze(name, schema, catalog, properties));
        tableBuilder.setConstraints(ConstraintAnalyzer.analyze(name, schema, catalog, properties));
        tableBuilder.setIndexes(IndexAnalyzer.analyze(name, schema, catalog, properties));

        return tableBuilder.build();
    }
}
