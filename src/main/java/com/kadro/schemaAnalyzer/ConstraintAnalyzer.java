package com.kadro.schemaAnalyzer;

import com.kadro.db.mysql.QueryHelper;
import com.kadro.schemaAnalyzer.intf.ConnectionProperties;
import com.kadro.schemaAnalyzer.intf.Constraint;
import com.sun.tools.internal.jxc.ap.Const;

import java.sql.PreparedStatement;
import java.util.*;
import java.util.stream.Collectors;

public class ConstraintAnalyzer {

    private ConstraintAnalyzer() {}

    private enum Columns {
        CONSTRAINT_TYPE("tc")
            , CONSTRAINT_NAME("tc")
            , COLUMN_NAMES() {
                @Override
                public String getSelector() {
                    return "GROUP_CONCAT(kcu.COLUMN_NAME ORDER BY COLUMN_NAME) as " + name();
                }
            }
            , REFERENCED_TABLE_NAME("kcu")
            , REFERENCED_COLUMN_NAME("kcu")
            , DELETE_RULE("rc")
            , UPDATE_RULE("rc")
        ;

        private String prefix;

        Columns() {}

        Columns(String prefix) {
            this.prefix = prefix;
        }

        public String getSelector() {
            StringBuilder selector = new StringBuilder();
            if (this.prefix != null) {
                selector.append(this.prefix).append(".");
            }
            selector.append(name());
            return selector.toString();
        }
    }

    public static Map<Constraint.Type, Collection<Constraint>> analyze(String name, String schema, String catalog, ConnectionProperties connectionProperties) {
        return QueryHelper.query(
                connectionProperties
                , getResultGenerator(name, schema, catalog)
                , getResultHandler()
        );
    }

    private static QueryHelper.ResultSetGenerator getResultGenerator(String name, String schema, String catalog) {
        return (connection) -> {
            StringBuffer sql = new StringBuffer("SELECT ");
            sql.append(
                    Arrays.stream(ConstraintAnalyzer.Columns.values())
                            .map(ConstraintAnalyzer.Columns::getSelector)
                            .collect(Collectors.joining(","))
            );
            sql.append(" FROM information_schema.TABLE_CONSTRAINTS tc ");
            sql.append("INNER JOIN information_schema.KEY_COLUMN_USAGE kcu ");
            sql.append("\tON tc.CONSTRAINT_SCHEMA  = kcu.CONSTRAINT_SCHEMA ");
            sql.append("    AND tc.CONSTRAINT_CATALOG = kcu.CONSTRAINT_CATALOG ");
            sql.append("    AND tc.TABLE_NAME = kcu.TABLE_NAME ");
            sql.append("    AND tc.CONSTRAINT_NAME = kcu.CONSTRAINT_NAME ");
            sql.append("LEFT OUTER JOIN information_schema.REFERENTIAL_CONSTRAINTS rc ");
            sql.append(" ON tc.CONSTRAINT_SCHEMA  = rc.CONSTRAINT_SCHEMA ");
            sql.append("    AND tc.CONSTRAINT_CATALOG = rc.CONSTRAINT_CATALOG ");
            sql.append("    AND tc.TABLE_NAME = rc.TABLE_NAME ");
            sql.append("    AND tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME ");
            sql.append("WHERE tc.TABLE_NAME = ? AND tc.TABLE_SCHEMA = ? AND tc.CONSTRAINT_CATALOG = ? ");
            sql.append("GROUP BY tc.CONSTRAINT_TYPE ");
            sql.append(", tc.CONSTRAINT_NAME ");
            sql.append(", kcu.REFERENCED_TABLE_NAME ");
            sql.append(", kcu.REFERENCED_COLUMN_NAME ");
            sql.append(", rc.DELETE_RULE");
            sql.append(", rc.UPDATE_RULE ");

            PreparedStatement statement = connection.prepareStatement(sql.toString());

            statement.setString(1, name);
            statement.setString(2, schema);
            statement.setString(3, catalog);

            return statement.executeQuery();
        };
    }

    private static QueryHelper.ResultSetHandler<Map<Constraint.Type, Collection<Constraint>>> getResultHandler() {
        return (resultSet) -> {

            Map<Constraint.Type, Collection<Constraint>> constraintsByType = new HashMap<>();

            while (resultSet.next()) {
                Constraint constraint = new Constraint.Builder()
                                            .setType(Constraint.Type.valueOf(resultSet.getString(Columns.CONSTRAINT_TYPE.name()).replace(" ","_")))
                                            .setName(resultSet.getString(ConstraintAnalyzer.Columns.CONSTRAINT_NAME.name()))
                                            .setColumnNames(Arrays.asList(resultSet.getString(Columns.COLUMN_NAMES.name()).split(",")))
                                            .setReferencedTableName(resultSet.getString(Columns.REFERENCED_TABLE_NAME.name()))
                                            .setReferencedColumnName(resultSet.getString(Columns.REFERENCED_COLUMN_NAME.name()))
                                            .setDeleteRule(resultSet.getString(Columns.DELETE_RULE.name()))
                                            .setUpdateRule(resultSet.getString(Columns.UPDATE_RULE.name()))
                                            .build();

                Collection<Constraint> constraints = constraintsByType.getOrDefault(constraint.getType(), new ArrayList<>());
                constraints.add(constraint);
                constraintsByType.put(constraint.getType(), constraints);
            }

            return constraintsByType;
        };
    }

}
