package com.kadro.schemaAnalyzer.intf;

import java.util.Collection;

public interface Index {

    enum Type {
        BTREE
    }

    Type getType();
    String getName();
    Collection<String> getColumnNames();

    class Builder {
        private Type type;
        private String name;
        private Collection<String> columnNames;

        public Builder() {}

        public Builder setType(Type type) {
            this.type = type;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setColumnNames(Collection<String> columnNames) {
            this.columnNames = columnNames;
            return this;
        }

        public Index build() {
            return new Index() {
                @Override
                public Type getType() {
                    return type;
                }

                @Override
                public String getName() {
                    return name;
                }

                @Override
                public Collection<String> getColumnNames() {
                    return columnNames;
                }
            };
        }
    }

}
