package com.kadro.schemaAnalyzer.intf;

import com.sun.tools.internal.jxc.ap.Const;

import java.util.Collection;

public interface Constraint {

    enum Type {
        PRIMARY_KEY
        , FOREIGN_KEY
        , UNIQUE
        ;
    }

    Type getType();
    String getName();
    Collection<String> getColumnNames();
    String getReferencedTableName();
    String getReferencedColumnName();
    String getDeleteRule();
    String getUpdateRule();

    class Builder {
        private Type type;
        private String name;
        private Collection<String> columnNames;
        private String referencedTableName;
        private String referencedColumnName;
        private String deleteRule;
        private String updateRule;

        public Builder setType(Type type) {
            this.type = type;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setColumnNames(Collection<String> columnNames) {
            this.columnNames = columnNames;
            return this;
        }

        public Builder setReferencedTableName(String referencedTableName) {
            this.referencedTableName = referencedTableName;
            return this;
        }

        public Builder setReferencedColumnName(String referencedColumnName) {
            this.referencedColumnName = referencedColumnName;
            return this;
        }

        public Builder setDeleteRule(String deleteRule) {
            this.deleteRule = deleteRule;
            return this;
        }

        public Builder setUpdateRule(String updateRule) {
            this.updateRule = updateRule;
            return this;
        }

        public Constraint build() {
            return new Constraint() {
                @Override
                public Type getType() {
                    return type;
                }

                @Override
                public String getName() {
                    return name;
                }

                @Override
                public Collection<String> getColumnNames() {
                    return columnNames;
                }

                @Override
                public String getReferencedTableName() {
                    return referencedTableName;
                }

                @Override
                public String getReferencedColumnName() {
                    return referencedColumnName;
                }

                @Override
                public String getDeleteRule() {
                    return deleteRule;
                }

                @Override
                public String getUpdateRule() {
                    return updateRule;
                }
            };
        }
    }
}
