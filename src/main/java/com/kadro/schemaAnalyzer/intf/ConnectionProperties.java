package com.kadro.schemaAnalyzer.intf;

public interface ConnectionProperties {

    String getDriver();
    String getHost();
    String getUsername();
    String getPassword();
    String getDatabase();

    public static ConnectionProperties build(String driver, String host, String username, String password, String database) {
        return new ConnectionProperties() {
            @Override
            public String getDriver() {
                return driver;
            }

            @Override
            public String getHost() { return host; }

            @Override
            public String getUsername() {
                return username;
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public String getDatabase() {
                return database;
            }
        };
    }

}
