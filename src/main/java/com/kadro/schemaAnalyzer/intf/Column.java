package com.kadro.schemaAnalyzer.intf;

import com.mysql.cj.MysqlType;

public interface Column {

    String getName();
    String getDefault();
    MysqlType getDataType();
    boolean isNullable();
    boolean isAutoIncrement();
    boolean isOnUpdate();
    boolean isUnsigned();
    int getPrecision();
    int getScale();

    class Builder {
        private String name;
        private boolean isNullable;
        private MysqlType dataType;
        private int precision;
        private int scale;
        private String extra;
        private String columnType;
        private String defaultValue;

        public Builder() { }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setDefault(String defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        public Builder setNullable(boolean nullable) {
            isNullable = nullable;
            return this;
        }

        public Builder setDataType(MysqlType dataType) {
            this.dataType = dataType;
            return this;
        }

        public Builder setPrecision(int precision) {
            this.precision = precision;
            return this;
        }

        public Builder setScale(int scale) {
            this.scale = scale;
            return this;
        }

        public Builder setExtra(String extra) {
            this.extra = extra;
            return this;
        }

        public Builder setColumnType(String columnType) {
            this.columnType = columnType;
            return this;
        }

        public Column build() {
            return new Column() {
                @Override
                public String getName() {
                    return name;
                }

                @Override
                public String getDefault() { return defaultValue; }

                @Override
                public boolean isNullable() {
                    return isNullable;
                }

                @Override
                public boolean isAutoIncrement() {
                    return extra != null && extra.toLowerCase().contains("auto_increment");
                }

                @Override
                public boolean isUnsigned() {
                    return columnType != null && columnType.toLowerCase().contains("unsigned");
                }

                @Override
                public boolean isOnUpdate() {
                    return extra != null && extra.toLowerCase().contains("on update");
                }

                @Override
                public MysqlType getDataType() {
                    return dataType;
                }

                @Override
                public int getPrecision() {
                    return precision;
                }

                @Override
                public int getScale() {
                    return scale;
                }
            };
        }
    }

}
