package com.kadro.schemaAnalyzer.intf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public interface Table {

    String getName();
    String getSchema();
    String getCatalog();
    Collection<Column> getColumns();
    Collection<Index> getIndexes();
    Map<Constraint.Type, Collection<Constraint>> getConstraintsByType();

    class Builder {

        private String name;
        private String schema;
        private String catalog;
        private Collection<Column> columns;
        private Map<Constraint.Type, Collection<Constraint>> constraintsByType;
        private Collection<Index> indexes;

        public Builder(String name, String schema, String catalog) {
            this.name = name;
            this.schema = schema;
            this.catalog = catalog;
            this.columns = new ArrayList<>();
            this.constraintsByType = new HashMap<>();
        }

        public Builder setColumns(Collection<Column> columns) {
            this.columns = columns;
            return this;
        }

        public Builder setConstraints(Map<Constraint.Type, Collection<Constraint>> constraintsByType) {
            this.constraintsByType = constraintsByType;
            return this;
        }

        public Builder setIndexes(Collection<Index> indexes) {
            this.indexes = indexes;
            return this;
        }

        public Table build() {
            return new Table() {

                @Override
                public String getName() {
                    return name;
                }

                @Override
                public String getSchema() {
                    return schema;
                }

                @Override
                public String getCatalog() {
                    return catalog;
                }

                @Override
                public Collection<Column> getColumns() {
                    return columns;
                }

                @Override
                public Collection<Index> getIndexes() { return indexes; }

                @Override
                public Map<Constraint.Type, Collection<Constraint>> getConstraintsByType() { return constraintsByType; }
            };
        }
    }
}
