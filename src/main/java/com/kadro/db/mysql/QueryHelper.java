package com.kadro.db.mysql;

import com.kadro.schemaAnalyzer.intf.ConnectionProperties;

import java.sql.*;

public class QueryHelper {

    public static <T> T query(ConnectionProperties properties, ResultSetGenerator generator, ResultSetHandler<T> handler) {
        Connection connection = null;
        try {
            Class.forName(properties.getDriver());
            connection = DriverManager.getConnection(properties.getHost(), properties.getUsername(), properties.getPassword());

            return handler.handle(generator.query(connection));
        } catch (Throwable th) {
            throw new RuntimeException(th);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Throwable ignore) {}
            }
        }
    }

    public interface ResultSetGenerator {
        public ResultSet query(Connection connection) throws SQLException;
    }

    public interface ResultSetHandler<T> {
        public T handle(ResultSet resultSet) throws SQLException;
    }

}
